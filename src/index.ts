import Discord from 'discord.js';

import { update_alerts, update_infos, sent_alerts_ids, sent_infos_ids } from './receiver';
import promiseRequest from "./promiseRequest";
const config_file = require("../config.json");

const client = new Discord.Client();
let website_token = "";
const send_discord_alerts = true;
const send_discord_info = true;
const alert_update_frequency = 1000 * 60 * 15; // every 30 minutes
const info_update_frequency = 1000 * 60 * 60 * 12; // every 12 hour


client.on('ready', () => {
    if (client.user) {
        client.user.setPresence({ activity: { name: `running v${process.env.npm_package_version}` } });
        console.log(`Connecté à discord en tant que ${client.user.tag}.`);
        client.generateInvite({ permissions: ['ADD_REACTIONS', 'MENTION_EVERYONE', 'VIEW_CHANNEL', 'EMBED_LINKS', 'SEND_MESSAGES', 'READ_MESSAGE_HISTORY'] })
            .then(value => {
                console.log("Lien d’invitation : " + value);
            });
        client.guilds.fetch(config_file.discord_guild_id)
            .then((value) => {
                if (value.systemChannel) {
                    update_alerts(value.systemChannel, website_token);
                    update_infos(value.systemChannel, website_token);
                }
                setInterval(() => {
                    if (value.systemChannel) {
                        if (send_discord_alerts)
                            update_alerts(value.systemChannel, website_token);
                    }
                }, alert_update_frequency);
                setInterval(() => {
                    if (value.systemChannel) {
                        if (send_discord_info)
                            update_infos(value.systemChannel, website_token);
                    }
                }, info_update_frequency);
            });
    }
});

client.on('message', msg => {
    if (msg.author.bot) return; // don’t read bot messages
    if (msg.guild && msg.guild.id === config_file.discord_guild_id) // if we’re in the right guild
    {
        if (msg.member && msg.member.roles && msg.member.roles.cache.get(config_file.discord_professor_group_id)) { // if it’s a professor
            if (msg.content.startsWith(config_file.prefix + 'info')) {
                let content = msg.content.substr(config_file.prefix.length + 5);
                let nextMonth = new Date(Date.now());
                nextMonth.setMonth(nextMonth.getMonth() + 1);

                let info = {
                    "title": `message de ${msg.member.displayName} (${msg.author.tag})`,
                    "content": content,
                    "expiration-date": nextMonth.toISOString(),
                };

                let body = JSON.stringify(info);
                promiseRequest(config_file.website + "/wp-json/amu-ecran-connectee/v1/information", {
                    method: 'POST', headers: {
                        "Authorization": 'Bearer ' + website_token,
                        'Content-length': Buffer.byteLength(body),
                        'Content-type': 'application/json'
                    }
                }, body)
                    .then((value) => JSON.parse(value))
                    .then(json => {
                        if (!json.id)
                            throw new Error();
                        sent_infos_ids.push(json.id);
                        msg.react("✅");
                    }).catch((reason) => {
                        console.error("api error : " + JSON.stringify(reason));
                        msg.channel.send("Erreur lors de l’envoi de l’information");
                    });

            }
            else {
                let codes: Array<string> = [];
                let roles: Array<string> = [];
                if (msg.mentions.everyone) {
                    codes.push("all");
                    roles.push('tout le monde');
                } else {
                    msg.mentions.roles.forEach((value, key) => {
                        for (const id in config_file.discord_studends_group_association) {
                            if (id === key) {
                                codes.push(config_file.discord_studends_group_association[id]);
                                roles.push(value.name);
                            }
                        }
                    });
                }

                if (codes.length > 0 && msg.channel.type === 'text') {
                    const expiration = new Date();
                    expiration.setDate(expiration.getDate() + 7); // one week
                    let strRoles = '';
                    roles.forEach((value) => {
                        strRoles += value;
                    });
                    if (strRoles.length + 100 > 280) {
                        strRoles = '';
                    }
                    let body = JSON.stringify({
                        'content': `Message de ${msg.member.displayName} sur le channel #${msg.channel.name} concernant ${strRoles}`,
                        'expiration-date': expiration.toISOString(),
                        'codes': codes
                    });
                    promiseRequest(config_file.website + '/wp-json/amu-ecran-connectee/v1/alert', {
                        method: 'POST',
                        headers: {
                            "Authorization": 'Bearer ' + website_token,
                            'Content-length': Buffer.byteLength(body),
                            'Content-type': 'application/json'
                        }
                    }, body)
                        .then(value => {
                            return JSON.parse(value);
                        })
                        .then((json) => {
                            if (!json.id)
                                throw new Error(json.data.status);
                            msg.react("✅");
                            sent_alerts_ids.push(json.id);

                        })
                        .catch((reason) => {
                            console.log("api error : " + JSON.stringify(reason));
                            msg.channel.send("Erreur lors de l’envoi de l’alerte");
                        });

                }
            }
        }
    }
    else if (msg.channel instanceof Discord.DMChannel)
        msg.reply("Veuillez m’utiliser sur le serveur !");
});

function getWordpressToken() {
    const body = `username=${config_file.username}&password=${config_file.password}`;
    promiseRequest(config_file.website + "/wp-json/jwt-auth/v1/token", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-length': Buffer.byteLength(body)
        }
    }, body)
        .then(value => JSON.parse(value))
        .then(json => {
            if (json.token) {
                website_token = json.token;
                console.log(`Connecté au site Wordpress en tant que ${json.user_display_name}.`);
            } else {
                if (json.data.status === 403)
                    console.error('Les informations de connexion wordpress sont incorrectes');
                else console.error(`Token non trouvé, contenu de la réponse : ${JSON.stringify(json)}`);
            }
        })
        .catch(reason => {
            console.error("erreur lors de la connexion au site wordpress, l’url (paramètre website) est-elle la bonne ? Code d’erreur : ", reason);
        });
}

getWordpressToken();
client.login(config_file.discord_token);