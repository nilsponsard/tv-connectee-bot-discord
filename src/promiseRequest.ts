import * as https from "https";
import { ClientRequest, IncomingMessage } from 'node:http';
export default function promiseRequest(url: string | import("url").URL, options?: https.RequestOptions, body?: string) {
    return new Promise<string>((resolve, reject) => {

        let callback = (res: IncomingMessage) => {
            let data = '';
            res.on('data', chunk => {
                data += chunk;
            });
            res.on("end", () => {
                resolve(data);
            });
        };
        let request: ClientRequest;

        if (options)
            request = https.request(url, options, callback);
        else
            request = https.request(url, callback);

        request.on("error", (e) => {
            reject(e);
        });

        if (body)
            request.write(body);
        request.end();
    });

}