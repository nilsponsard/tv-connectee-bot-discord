import * as fs from 'fs';
import Discord from 'discord.js';
import promiseRequest from "./promiseRequest";
const config_file = require("../config.json");



let last_alert_id = -1;
let last_info_id = -1;

if (fs.existsSync('./last_alert')) {
    let buf = fs.readFileSync('./last_alert');
    last_alert_id = buf.readInt32LE();
}
if (fs.existsSync('./last_info')) {
    let buf = fs.readFileSync('./last_info');
    last_info_id = buf.readInt32LE();
}

export let sent_alerts_ids: Array<string> = [];
export let sent_infos_ids: Array<string> = [];



interface Info {
    id: string,
    author: {},
    title: string,
    creation_date: string,
    expirationDate: string,
    content: string,
    type: string,
    adminId: unknown;
}

export function update_infos(chan: Discord.TextChannel, token: string) {
    let offset_id = last_info_id;
    if (offset_id < 0) {
        offset_id = 0;
    }
    promiseRequest(config_file.website + '/wp-json/amu-ecran-connectee/v1/information?limit=100&offset=' + offset_id, {
        method: 'GET', headers: {
            "Authorization": 'Bearer ' + token,
        }
    })
        .then(value => {
            return JSON.parse(value) as Array<Info>;
        })
        .then(value => {
            if (value.length > 0) {
                chan.startTyping();
                value.forEach((info) => {
                    const id = parseInt(info.id, 10);
                    if (id > last_info_id && sent_infos_ids.indexOf(info.id) === -1) {
                        let embed = new Discord.MessageEmbed();
                        embed.setTitle(`Nouvelle info`);
                        if (info.type === 'text')
                            embed.addField('message : ', info.content);
                        chan.send(embed);
                        last_info_id = id;
                    }
                });
                chan.stopTyping(true);
                let buf = Buffer.alloc(4);
                buf.writeInt32LE(last_info_id);
                fs.writeFileSync('./last_info', buf);
            }
        });
}

interface Alert {
    id: string,
    author: { id: string, name: string; },
    content: string,
    creation_date: string,
    expirationDate: string,
    codes: Array<{ id: string, type: string, title: string, code: string; }>,
    forEveryone: unknown,
    adminId: unknown;
}

export function update_alerts(chan: Discord.TextChannel, token: string) {

    promiseRequest(config_file.website + '/wp-json/amu-ecran-connectee/v1/alert', {
        method: 'GET', headers: {
            "Authorization": 'Bearer ' + token,
        }
    })
        .then(value => {
            return JSON.parse(value) as Array<Alert>;
        })
        .then(value => {
            if (value.length > 0) {
                chan.startTyping();

                value.forEach((alerte) => {
                    const id = parseInt(alerte.id, 10);
                    if (id > last_alert_id && sent_alerts_ids.indexOf(alerte.id) === -1) {
                        let mention = `@everyone`;
                        let everyone = true;
                        for (const role_id in config_file.discord_studends_group_association) {
                            alerte.codes.forEach((v) => {
                                if (v.code === config_file.discord_studends_group_association[role_id]) {
                                    if (everyone)
                                        mention = '';
                                    mention += `<@&${role_id}>`;
                                }
                            });
                        }
                        let embed = new Discord.MessageEmbed();
                        embed.setFooter(`de ${alerte.author.name}`);
                        embed.setTitle(`Nouvelle alerte`);
                        embed.addField('message : ', alerte.content);
                        chan.send(mention, embed);
                        last_alert_id = id;
                    }
                });
                let buf = Buffer.alloc(4);
                buf.writeInt32LE(last_alert_id);
                fs.writeFileSync('./last_alert', buf);
            }
            chan.stopTyping(true);

        });
}