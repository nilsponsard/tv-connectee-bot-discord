const inputDiscordKey = document.getElementById("discordKey") as HTMLInputElement;
const inputPrefix = document.getElementById("prefix") as HTMLInputElement;
const inputWebsite = document.getElementById("website") as HTMLInputElement;
const inputUsername = document.getElementById("username") as HTMLInputElement;
const inputPassword = document.getElementById("password") as HTMLInputElement;
const inputGuildID = document.getElementById("guildID") as HTMLInputElement;
const inputProfessorID = document.getElementById("professorID") as HTMLInputElement;
const divStudents = document.getElementById("students") as HTMLDivElement;
const buttonAdd = document.getElementById("add") as HTMLButtonElement;
const form = document.getElementById("form") as HTMLInputElement;


class Group {
    inputGroupID: HTMLInputElement;
    inputAde: HTMLInputElement;
    constructor() {
        const div = document.createElement("div");

        const labelGroup = document.createElement("label");
        labelGroup.innerText = 'ID du groupe discord : ';
        this.inputGroupID = document.createElement("input");
        this.inputGroupID.type = 'text';
        labelGroup.appendChild(this.inputGroupID);

        div.appendChild(labelGroup);

        const labelAde = document.createElement("label");
        labelAde.innerText = 'Code ade : ';
        this.inputAde = document.createElement("input");
        this.inputAde.type = 'text';
        labelAde.appendChild(this.inputAde);

        div.appendChild(labelAde);
        divStudents.appendChild(div);
    }


}

const groups: Array<Group> = [];


function addGroup() {
    groups.push(new Group());
}
addGroup();
buttonAdd.addEventListener('click', addGroup);

form.addEventListener('submit', (ev) => {
    ev.preventDefault();
    let objStudents = {} as { [key: string]: string; };
    for (const el of groups) {
        objStudents[el.inputGroupID.value.trim()] = el.inputAde.value.trim();
    }

    let objsave = {
        "discord_token": inputDiscordKey.value.trim(),
        "prefix": inputPrefix.value.trim(),
        "website": inputWebsite.value.trim(),
        "username": inputUsername.value.trim(),
        "password": inputPassword.value.trim(),
        "discord_guild_id": inputGuildID.value.trim(),
        "discord_professor_group_id": inputProfessorID.value.trim(),
        "discord_studends_group_association": objStudents

    };

    let out = JSON.stringify(objsave);
    let file = new Blob([out], { type: "application/javascript" });
    // window.navigator.msSaveOrOpenBlob(file, "script.js");
    let a = document.createElement("a");
    let url = URL.createObjectURL(file);
    a.href = url;
    a.style.display = "none";
    a.download = "config.json";
    document.body.appendChild(a);
    a.click();
    setTimeout(function () {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
    }, 0);

});
