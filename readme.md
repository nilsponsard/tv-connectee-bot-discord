


## fonctionnalitées
### !info <contenu de l’information> 
Accessible que par les membres de `discord_professor_group_id`, envoie une information sur le site. Taille maximale du contenu de 280 caractères.

### envoi d’alertes
Un membre appartenant au groupe discord_professor_group_id peut envoyer une alerte en mentionnant @everyone ou un groupe dans discord_studends_group_association.

### réception d’alertes 
Le bot envoie les nouvelles alertes dans le channel principal du serveur en mentionnant les groupes concernés.

### réception d’informations
Le bot envoie les nouvelles alertes dans le channel principal du serveur.

## obtenir une clef discord
- Aller sur le site de discord en se connectant : [https://discord.com/developers/applications](https://discord.com/developers/applications)
- créer une nouvelle application (New Application)
- aller dans le sous-menu bot
- copier le token

## obtenir les identifiants (ID) des groupes/serveurs discord
- vous avez besoin d’être en mode développer sur discord
    - Paramètres discord > Apparence > Avancés > Mode développeur
- pour obtenir l’ID faites un click droit sur l’élément puis click gauche sur copier l’identitifiant
- vous pouvez obtenir l’identifiant d’un rôle dans les paramètres du serveur onglet rôle, click droit sur le rôle puis copier l’identifiant
- vous pouvez obtenir l’identifiant du serveur en faisant click droit sur l’icone du serveur puis copier l’identifiant
## configuration du bot
- Générer le fichier config.json grâce à ce site : [https://nilsponsard.gitlab.io/tv-connectee-bot-discord/](https://nilsponsard.gitlab.io/tv-connectee-bot-discord/)
- placer le fichier généré dans le dossier du bot
- démarrer avec 
```
npm install && npm run build && npm run start
```

### information de configuration
format du `config.json`.

```json
{
  "discord_token": "<votre-token-ici>",
  "prefix": "!",
  "website": "<url du site>",
  "username": "<wordpress username>",
  "password": "<wordpress password>",
  "discord_guild_id": "<l’id du serveur discord>",
  "discord_professor_group_id": "<id du rôle discord des professeurs>",
  "discord_studends_group_association": {
    "<id du rôle discord d’un groupe d’étudiant>": "<le code ade correspondant>"
  }
}
```
